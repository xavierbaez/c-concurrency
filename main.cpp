#include <iostream>
#include <thread>
#include <vector>
#include <chrono>

// Function to compute Fibonacci numbers (CPU-intensive task)
// Author @ Xavier Baez <xavierbaez@gmail.com
unsigned long long fibonacci(unsigned int n) {
    if (n <= 1) {
        return n;
    }
    unsigned long long a = 0, b = 1, c;
    for (unsigned int i = 2; i <= n; ++i) {
        c = a + b;
        a = b;
        b = c;
    }
    return c;
}

// Function to run in each thread
void worker(unsigned int n, unsigned int id) {
    // Keep the CPU busy for a while
    while (true) {
        unsigned long long result = fibonacci(n);
        std::cout << "Thread " << id << " calculated Fibonacci(" << n << ") = " << result << std::endl;
    }
}

int main() {
    // Determine the number of logical cores available
    unsigned int num_threads = std::thread::hardware_concurrency();
    std::cout << "Number of logical cores: " << num_threads << std::endl;

    // Vector to hold our threads
    std::vector<std::thread> threads;

    // Start a thread for each logical core
    for (unsigned int i = 0; i < num_threads; ++i) {
        threads.push_back(std::thread(worker, 40, i));  // Adjust '40' for a more/less intensive task
    }

    // Join the threads (this will never happen in this example as the threads run indefinitely)
    for (auto& thread : threads) {
        thread.join();
    }

    return 0;
}